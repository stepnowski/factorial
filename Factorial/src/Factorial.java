
public class Factorial
{
	public int determineFatorial(int number)
	{
		int n = number;
		int answer = 1;
		
		//0! = 1 special case
		if (n==0)
		{
			return answer;
		}
		// n*(n-1)*(n-2)*...*(2)*(1)
		else
		{
			while(n>1)
			{
				answer*= n;
				n--;
			}
			return answer;
		}
	}
}
