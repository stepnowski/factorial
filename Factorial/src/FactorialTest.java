import java.util.Scanner;

public class FactorialTest
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		Factorial myfactorial = new Factorial();
		int number;
		int answer;
		
		System.out.print("Enter a number to factorial:");
		number = input.nextInt();
		
		answer = myfactorial.determineFatorial(number);
		
		System.out.printf("%d! = %d", number, answer);				

	}//end main method
}
